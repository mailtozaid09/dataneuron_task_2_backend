import express from "express";
import { user } from "./user/index.js";

const router = express.Router();

// USER Routes * /api/user/*

router.use("/user", user);

export const routes = router;
