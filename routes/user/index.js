import express from "express";

import { addUser, deleteAllUser, deleteUser, getUsers, resetCount, updateUser } from "../../controllers/user/index.js";

const router = express.Router();

// ROUTES * /api/v1/user/

router.get("/", getUsers);
router.post("/", addUser);
router.put("/:id", updateUser);
router.delete("/:id", deleteUser);

router.delete("/delete_users", deleteAllUser);

router.post("/reset_count", resetCount);

export const user = router;
