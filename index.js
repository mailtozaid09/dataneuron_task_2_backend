import express from 'express';
import cors from 'cors';
import connectDB from './config/db/index.js'
import { routes } from './routes/index.js';

const app = express();

// * Database connection
connectDB()

// * Cors
app.use(cors());
app.options("*", cors());


// * Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


// * Api routes
app.use("/api/v1", routes);


app.use("*", (req, res) => {
    res.status(400).send({ success: false, message: 'Route not found' });
});

let PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`Server is running on PORT ${PORT}`));
