import mongoose from 'mongoose';

const userSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        userName: {
            type: String,
            required: true,
        },
        mobileNumber: {
            type: String,
            required: true,
        },
    },
    {
        timestamps: true
    }
);

userSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

userSchema.set('toJSON', {
    virtuals: true,
});

export const User =  mongoose.model('User', userSchema);

