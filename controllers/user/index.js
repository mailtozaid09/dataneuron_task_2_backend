import { User } from "../../models/user/index.js";

let addUserCounter = 0;
let updateUserCounter = 0;


export const getUsers = async (req, res) => {
    try {
        const user = await User.find()
        if (!user) {
            res.status(200).json({ success: false, data: [] })
        }
        res.status(200).json({ success: true, data: user, count: {add_count: addUserCounter, update_count: updateUserCounter} })
    } catch (err) {
        console.log("err >>> ", err);
        res.status(400).send({ success: false, message: err });
    }
};

export const addUser = async (req, res) => {
    try {
        let user = new User({
            name: req.body.name,
            userName: req.body.userName,
            mobileNumber: req.body.mobileNumber,
        })
        user = await user.save();
    
        if (!user) {
            res.status(400).json({ success: false, message: 'The user cannot be created!', })
        }

        addUserCounter++;
        res.status(200).json({ success: true, message: 'The user created successfully!', data: user })
    } catch (err) {
        console.log("err >>> ", err);
        res.status(400).send({ success: false, message: err });
    }
};


export const updateUser = async (req, res) => {
    try {

        const userExist = await User.findById(req.params.id);
        
        if(!userExist) {
            return res.status(400).send({success: false, data: userExist, message: 'The user does not exist!',})
        }

        const user = await User.findByIdAndUpdate(
            req.params.id,
            {
                name: req.body.name,
                userName: req.body.userName,
                mobileNumber: req.body.mobileNumber,
            },
            { new: true}
        )
    
        if (!user) {
            res.status(400).json({ success: false, message: 'The user cannot be updated!', })
        }
        updateUserCounter++;
        res.status(200).json({ success: true, message: 'The user updated successfully!', data: user })
    } catch (err) {
        console.log("err >>> ", err);
        res.status(400).send({ success: false, message: err });
    }
};

export const deleteUser = async (req, res) => {
    try {
        User.findByIdAndDelete(req.params.id)
        .then(user =>{
            if(user) {
                return res.status(200).json({success: true, message: 'The user deleted successfully!',})
            } else {
                return res.status(404).json({success: false , message: 'The user cannot be deleted!',})
            }
        }).catch(err=>{
            return res.status(500).json({success: false, error: err}) 
        })
    } catch (err) {
        console.log("err >>> ", err);
        res.status(400).send({ success: false, message: err });
    }
};


export const deleteAllUser = async (req, res) => {
    try {
        const result = await User.deleteMany({});
    
        if (result.deletedCount > 0) {
          res.status(200).json({ success: true, message: 'All user deleted successfully' });
        } else {
          res.status(404).json({ success: false, message: 'No user to delete' });
        }
    } catch (err) {
        console.log("err >>> ", err);
        res.status(400).send({ success: false, message: err });
    }
};


export const resetCount = async (req, res) => {
    try {
        addUserCounter = 0
        updateUserCounter = 0
        res.status(200).json({ success: true, message: 'Count reset successfully'});
    } catch (err) {
        console.log("err >>> ", err);
        res.status(400).send({ success: false, message: err });
    }
};



